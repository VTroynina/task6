let data = {
    random: {
        priceRandom: 1800
    },
    cute: {
        priceCute: 2500,
        cuteTypes: {
            shark: 1000,
            dogge: 500,
            dino: 400
        }
    },
    special: {
        priceSpecial: 1800,
        specialTypes: {
            pockets: 400,
            hood: 500,
            print: 600
        }
    }
};

let select;
let radios;
let checkboxes;
let col;
function changePrice() {
    let r = document.getElementById("result");
    let radioDiv = document.getElementById("radioDiv");
    let checkDiv = document.getElementById("checkDiv");
    let resultPrice = 0;

    if (select.value === "1") {
        radioDiv.style.display = "none";
        checkDiv.style.display = "none";
        resultPrice += data.random.priceRandom;
    }
    if (select.value === "2") {
        checkDiv.style.display = "none";
        radioDiv.style.display = "block";
        resultPrice += data.cute.priceCute;
        radios.forEach(function (radio) {
            if (radio.checked) {
                resultPrice += data.cute.cuteTypes[radio.value];
            }
        });
    }
    if (select.value === "3") {
        radioDiv.style.display = "none";
        checkDiv.style.display = "block";
        resultPrice += data.special.priceSpecial;
        checkboxes.forEach(function (check) {
            if (check.checked) {
                resultPrice += data.special.specialTypes[check.value];
            }
        });
    }
    if (/^[1-9][0-9]*$/.test(col.value)) {
        r.innerHTML = "Итого: " + resultPrice * col.value;
    } else {
        r.innerHTML = "Итого: введите корректное кол-во";
    }
}

window.addEventListener("DOMContentLoaded", function () {
    select = document.getElementById("myselect");
    radios = document.getElementsByName("myradio");
    checkboxes = document.getElementsByName("mycheck");
    col = document.getElementById("value");
    select.addEventListener("change", changePrice);
    radios.forEach(function (radio) {
        radio.addEventListener("change", changePrice);
    });
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", changePrice);
    });
    col.addEventListener("change", changePrice);
    changePrice();
});
